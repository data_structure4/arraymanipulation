import java.util.Scanner;

public class Lab1_2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int[] arr = new int[N];
        DuplicateZero(arr);
    }

    public static void DuplicateZero(int[]arr) {
        Scanner sc = new Scanner(System.in);
        for(int i=0; i<arr.length; i++){
            arr[i] = sc.nextInt();
        }

        for(int i = 0;i<arr.length;i++){
            if(arr[i]==0){
                for(int j = arr.length-1; j>i; j--){
                    arr[j] = arr[j-1];
                }
                i++;
            }
        }
        System.out.println("Output");
        for (int i = 0;i<arr.length;i++){
            System.out.println(arr[i]+"");
        }
    } 
}
